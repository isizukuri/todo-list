app.service('dataService', ['$http', function ($http) {
    var urlBase = '/jobs';
    this.getJobsFromServer = function () {
        return $http.get(urlBase);
    };
    this.postToServer = function (json) {
        return $http.post(urlBase, json);
    };
}]);
