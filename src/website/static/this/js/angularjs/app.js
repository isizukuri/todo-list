var app = angular.module('JobListApp', []);

app.config(['$interpolateProvider', function($interpolateProvider) {
'use strict';
    // Compatibility with Jinja2/Django templates.
    $interpolateProvider.startSymbol("{$");
    $interpolateProvider.endSymbol("$}");
}]);
