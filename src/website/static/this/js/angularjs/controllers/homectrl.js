app.controller('HomeCtrl', ['$scope', 'dataService', function($scope, dataService) {
    var getActualJobs = function(){
        return JSON.parse(localStorage.getItem('actual_jobs'))
    };

    var getDeletedJobs = function(){
        return JSON.parse(localStorage.getItem('deleted_jobs'))
    };

    // Trying to initialize actual_jobs and deleted_jobs from local storage
    $scope.actual_jobs = getActualJobs() || {};
    var ctrl = this;
    ctrl.deleted_jobs = getDeletedJobs() || {};

    ctrl.jobs_to_update = {};

    ctrl.saveActualJobs = function(){
        stringified_jobs = JSON.stringify($scope.actual_jobs);
        return localStorage.setItem('actual_jobs', stringified_jobs);
    };

    ctrl.saveDeletedJobs = function(jobs_to_delete){
        stringified_jobs = JSON.stringify(jobs_to_delete);
        return localStorage.setItem('deleted_jobs', stringified_jobs);
    };

    $scope.addJob = function(){
        if ($scope.form.$valid){
            var job = {
                created_at: Math.floor(Date.now() / 1000),
                is_finished: false,
                description: $scope.new_job,
            };
            job_key = job.created_at.toString();
            $scope.actual_jobs[job_key] = (job);
            ctrl.saveActualJobs($scope.actual_jobs);
            $scope.new_job = "";
            $scope.form.$setPristine();
            return true;
        }
    };

    $scope.deleteJob = function(job){
        job_key = job.created_at.toString();
        ctrl.deleted_jobs[job_key] = job;
        delete $scope.actual_jobs[job_key];
        ctrl.saveActualJobs($scope.actual_jobs);
        ctrl.saveDeletedJobs(ctrl.deleted_jobs);
        return true;
    };

    $scope.toggleJobStatus = function(job){
        if (job.is_finished){
            job.is_finished = false;
            delete job.finished_at;
        }
        else {
            job.is_finished = true;
            job.finished_at = Math.floor(Date.now() / 1000);
        };
        ctrl.saveActualJobs()
        return true;
    };

    $scope.synchronize = function(){
        getJobsFromServer(postToServer);
        ctrl.saveActualJobs();
    };

    var postToServer = function(){
        json = insertJobsOnServer();
        json = updateServerJobs(json);
        console.log(ctrl.jobs_to_update);
        json = deleteServerJobs(json);
        dataService.postToServer(json)
            .then(
                function(response){
                    if (response.data.update_status == 'OK') {
                        ctrl.jobs_to_update = {};
                        }
                    if (response.data.delete_status == 'OK') {
                        ctrl.deleted_jobs = {};
                        }
                },
                function(error){
                }
            );
    };

    var getJobsFromServer = function(callback) {
        dataService.getJobsFromServer()
            .then(
                function(response) {
                    incoming_jobs = response.data.jobs;
                    for (i in incoming_jobs){
                        got_job = incoming_jobs[i]
                        var job_key = got_job.created_at;
                        var new_job = {
                            created_at: job_key,
                            is_finished: got_job.is_finished,
                            description: got_job.description,
                            finished_at: got_job.finished_at,
                            };
                        if ($scope.actual_jobs.hasOwnProperty(job_key)) {
                            ctrl.jobs_to_update[job_key] = $scope.actual_jobs[job_key];
                            }
                        else if (!ctrl.deleted_jobs.hasOwnProperty(job_key)) {
                            $scope.actual_jobs[job_key] = new_job;
                            ctrl.saveActualJobs($scope.actual_jobs);
                        };
                    };
                    callback();
                },
                function(errordata) {
                    console.log("Error while getting jobs from server");
                }
        );
    };

    var insertJobsOnServer = function() {
        outgoing_jobs = [];
        for (key in $scope.actual_jobs){outgoing_jobs.push($scope.actual_jobs[key])};
        json = {"new_jobs": outgoing_jobs};
        return json
    };

    var updateServerJobs = function(json) {
        outgoing_jobs = [];
        for (key in ctrl.jobs_to_update){outgoing_jobs.push(ctrl.jobs_to_update[key])};
        json["jobs_to_update"] = outgoing_jobs;
        return json;
    };

    var deleteServerJobs = function(json) {
        outgoing_jobs = [];
        for (key in ctrl.deleted_jobs){outgoing_jobs.push(ctrl.deleted_jobs[key])};
        json["jobs_to_delete"] = outgoing_jobs;
        return json
    };
}]);
