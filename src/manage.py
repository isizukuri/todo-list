#!/usr/bin/env python3

"""
Special project manager as the ``manage.py`` in Django projects.
"""

import os
import re
import sys
import imp
import argparse

from migrate.versioning import api
from basic.wsgi import app, db


def runserver(args):
    """Run flask server."""
    host = '127.0.0.1'
    port = 5000

    if not re.findall(r'[0-9]+(?:\.[0-9]+){3}:[0-9]+', args.fullhost):
        print('Error! ', sys.stderr)
        return False

    host, port = args.fullhost.split(':')
    app.run(debug=app.config['DEBUG'], host=host, port=int(port))

    return True


def makemigrations(args):
    """Create/update migrations in SQLAlchemy."""
    v = api.db_version(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR']
    ) + 1

    migration = '{}{}'.format(
        app.config['SQLALCHEMY_MIGRATE_DIR'],
        '/versions/{}_migration.py'.format(v)
    )

    tmp_module = imp.new_module('old_model')
    old_model = api.create_model(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR']
    )

    exec(old_model, tmp_module.__dict__)
    script = api.make_update_script_for_model(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR'],
        tmp_module.meta,
        db.metadata
    )

    open(migration, "wt").write(script)
    api.upgrade(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR']
    )

    print('New migration saved as {}.'.format(migration))
    print('Current database version: {}.'.format(str(api.db_version(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR']
    ))))


def createdb(args):
    """Create database in SQLAlchemy."""
    db.create_all()
    if not os.path.exists(app.config['SQLALCHEMY_MIGRATE_DIR']):
        api.create(app.config['SQLALCHEMY_MIGRATE_DIR'], 'database repository')
        api.version_control(
            app.config['SQLALCHEMY_DATABASE_URI'],
            app.config['SQLALCHEMY_MIGRATE_DIR']
        )
    else:
        api.version_control(
            app.config['SQLALCHEMY_DATABASE_URI'],
            app.config['SQLALCHEMY_MIGRATE_DIR'],
            api.version(app.config['SQLALCHEMY_MIGRATE_DIR'])
        )

    print('Database created.')


def migrate(args):
    """Apply migration."""
    v = api.db_version(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR']
    )

    api.downgrade(
        app.config['SQLALCHEMY_DATABASE_URI'],
        app.config['SQLALCHEMY_MIGRATE_DIR'],
        v - 1 if v - 1 >= 0 else 0
    )

    print('Current database version: {}.'.format(str(
        api.db_version(
            app.config['SQLALCHEMY_DATABASE_URI'],
            app.config['SQLALCHEMY_MIGRATE_DIR']
        )
    )))


def parse_args():
    """Create parser of the command line."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # The ``runserver`` command:
    # .. runserver
    # .. runserver host:ip
    parser_runserver = subparsers.add_parser('runserver')
    parser_runserver.add_argument(
        'fullhost', nargs='?',
        default='127.0.0.1:5000'
        )
    parser_runserver.set_defaults(func=runserver)

    # The ``makemigrations`` command.
    parser_makemigrations = subparsers.add_parser('makemigrations')
    parser_makemigrations.add_argument(
        'model', nargs='?',
        default=None
        )
    parser_makemigrations.set_defaults(func=makemigrations)

    # The ``createdb`` command.
    parser_createdb = subparsers.add_parser('createdb')
    parser_createdb.set_defaults(func=createdb)

    # The ``migrate`` command.
    parser_createdb = subparsers.add_parser('migrate')
    parser_createdb.set_defaults(func=migrate)

    return parser.parse_args()


def main():
    args = parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
