from basic.views import main, api
from basic.wsgi import app


app.add_url_rule('/', view_func=main.HomeView.as_view('home'))
app.add_url_rule('/jobs', view_func=api.JobApi.as_view('job_api'))
