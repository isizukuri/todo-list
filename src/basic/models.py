import time
from datetime import datetime
from sqlalchemy.dialects.postgresql import INET as IPv4

from basic.wsgi import db


class Job(db.Model):
    """Job model."""
    __tablename__ = 'jobs'
    description = db.Column(db.String(128))
    created_at = db.Column(
        db.TIMESTAMP,
        default=datetime.utcnow,
        primary_key=True
        )
    finished_at = db.Column(db.TIMESTAMP, default=None)
    is_finished = db.Column(db.Boolean, default=False)
    user_ip = db.Column(IPv4, primary_key=True)
    user_browser = db.Column(
        db.String(length=25),
        primary_key=True,
        )

    def __init__(
            self, description, user_ip, user_browser,
            created_at, is_finished, finished_at=None,
            ):
        self.description = description
        self.user_ip = user_ip
        self.user_browser = user_browser
        self.created_at = self._from_timestamp(created_at)
        self.finished_at = self._from_timestamp(finished_at)
        self.is_finished = is_finished

    def __repr__(self):
        if self.user_ip is None:
            return '<Job: {0}>'.format(self.description)
        status = 'finished' if self.is_finished else 'open'
        return '<{0} job: {1} by {2}>'.format(
            status, self.description, self.user_ip)

    def to_json(self):
        """..."""
        json_job = {
            "description": self.description,
            "created_at": self._to_timestamp(self.created_at),
            "is_finished": self.is_finished,
            "finished_at": self._to_timestamp(self.finished_at),
        }
        return json_job

    def delete(self):
        """..."""
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception:
            db.session.rollback()

    def save(self):
        """..."""
        try:
            db.session.add(self)
            db.session.commit()
        except Exception:
            db.session.rollback()

    @staticmethod
    def _to_timestamp(timestamp=None):
        """..."""
        if timestamp:
            return int(time.mktime(timestamp.timetuple()))
        else:
            return None

    @staticmethod
    def _from_timestamp(timestamp=None):
        """..."""
        if timestamp:
            return datetime.fromtimestamp(timestamp)
        else:
            return None
