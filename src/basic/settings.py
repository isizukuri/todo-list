import os
import sys

# The root directory of the flask project.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# The root directory of the project (the lowest level).
BASE_DIR_UP = os.path.dirname(BASE_DIR)

# The path to the server configuration - use `local_settings.py` file where
# you can override the `settings.py` parameters.
sys.path.insert(1, os.path.join(BASE_DIR_UP, 'world', 'etc'))

DEBUG = True

# CSRF
CSRF_ENABLED = True
SECRET_KEY = '78as81723hkj213gjhf1hjasd576c78yuicyii099ge21763789213798d'

# Flask config.
# ... class flask.Flask(import_name, static_path=None, static_url_path=None,
#         static_folder='static', template_folder='templates',
#         instance_path=None, instance_relative_config=False)
STATIC_URL_PATH = '/static'
STATIC_FOLDER = os.path.join(BASE_DIR, 'website', 'static')
TEMPLATE_FOLDER = os.path.join(BASE_DIR, 'website', 'templates')

# DATABASE
# About SQLAlchemy: https://pythonhosted.org/Flask-SQLAlchemy/config.html
SQLALCHEMY_MIGRATE_DIR = os.path.join(BASE_DIR_UP, 'world', 'dev', 'migrations')
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = ('postgresql://{username}:{password}@{hostandport}/'
    '{database}'.format(
        username='todolist',
        password='todolist',
        hostandport='localhost',
        database='todolist'
    ))


# Loading extension parameters of standard configurations.
# Used to load various options on the server and on your local computer.
# ** If options are not loaded this point is ignored.
try:
    from local_settings import *
except ImportError:
    pass
