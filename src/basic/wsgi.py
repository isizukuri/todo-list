"""
WSGI config for project.
It exposes the WSGI callable as a module-level variable named ``app``.

For more information on this file, see:
http://flask.pocoo.org/docs/0.10/quickstart/

"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object('basic.settings')

# App settings by the parameters that specified in ``basic.settings``.
for opt in app.config:
    attr = opt.lower()
    if hasattr(app, attr):
        try:
            setattr(app, attr, app.config[opt])
        except AttributeError:
            pass

db = SQLAlchemy(app)

from basic import urls, models, staticviews
