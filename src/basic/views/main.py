from flask import render_template
from flask.views import MethodView


class HomeView(MethodView):
    """Show homepage."""
    methods = ('GET', )

    def get_context(self):
        """Standard context."""
        return {
            'title': 'TODO List',
        }

    def get_template_name(self):
        """Get teplate name."""
        return 'this/home.jinja'

    def render_template(self, context):
        """Render template."""
        return render_template(self.get_template_name(), **context)

    def get(self):
        """Show homepage."""
        context = self.get_context()

        return self.render_template(context)
