from datetime import datetime
from flask.views import MethodView
from flask import jsonify, request

from basic.models import Job


class JobApi(MethodView):
    """Smthng like RESTFul API for Jobs."""
    def get(self):
        """Returns JSON with all jobs."""
        user_ip = self._get_user_ip()
        user_browser = self._get_user_browser()
        queryset = Job.query.filter_by(
            user_ip=user_ip,
            user_browser=user_browser)
        response = jsonify({"jobs": [job.to_json() for job in queryset]})
        return response

    def post(self):
        """Saves every getted job and returns status."""
        response = {}
        try:
            new_jobs = request.get_json()['new_jobs']
            self._job_processor(self._job_create, new_jobs)
            response['save_status'] = "OK"
        except Exception as error:
            response['save_status'] = str(error)
        try:
            jobs_to_update = request.get_json()['jobs_to_update']
            self._job_processor(self._job_update, jobs_to_update)
            response['update_status'] = "OK"
        except Exception as error:
            response['update_status'] = repr(error)
        try:
            jobs_to_delete = request.get_json()['jobs_to_delete']
            self._job_processor(self._job_delete, jobs_to_delete)
            response['delete_status'] = "OK"
        except Exception as error:
            response['delete_status'] = repr(error)
        return jsonify(response)

    def _job_processor(self, handler, job_list):
        """Applies handler to every getted job."""
        result = [handler(job) for job in job_list]
        return result

    def _job_delete(self, job):
        """..."""
        try:
            job_to_delete = Job.query.filter_by(
                created_at=datetime.fromtimestamp(job['created_at']),
                user_ip=self._get_user_ip(),
                user_browser=self._get_user_browser(),
                ).one()
            job_to_delete.delete()
        except Exception:
            pass

    def _job_update(self, job):
        """..."""
        job_to_update = Job.query.filter_by(
            created_at=datetime.fromtimestamp(job['created_at']),
            user_ip=self._get_user_ip(),
            user_browser=self._get_user_browser(),
            ).one()
        job_to_update.is_finished = job['is_finished']
        if 'finished_at' in job.keys():
            job_to_update.finished_at = datetime.fromtimestamp(
                job['finished_at']
                )
        else:
            job_to_update.finished_at = None
        job_to_update.save()

    def _job_create(self, job):
        """..."""
        already_present = Job.query.filter_by(
            created_at=datetime.fromtimestamp(job['created_at']),
            user_ip=self._get_user_ip(), user_browser=self._get_user_browser(),
            ).all()
        if not already_present:
            new_job = Job(
                user_ip=self._get_user_ip(),
                user_browser=self._get_user_browser(),
                **job)
            new_job.save()

    def _get_user_ip(self):
        """..."""
        if 'X-Forwarded-For' in request.headers:
            user_ip = request.headers.getlist("X-Forwarded-For")[0] \
                .rpartition(' ')[-1]
        elif 'CLIENT_IP' in request.headers:
            user_ip = request.headers['CLIENT_IP']
        elif 'REMOTE_ADDR' in request.environ:
            user_ip = request.environ['REMOTE_ADDR']
        else:
            user_ip = request.remote_addr or '8.8.8.8'  # for test purposes
        return user_ip

    def _get_user_browser(self):
        """..."""
        return request.user_agent.browser
