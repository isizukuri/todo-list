from flask import send_from_directory, render_template
from basic.wsgi import app


@app.errorhandler(404)
def page_not_found(e):
    """Page not found."""
    response = {
        'title': '404 Page Not Found',
        'message': '404 Page Not Found',
    }

    return render_template('sys.jinja', **response), 404


@app.errorhandler(500)
def internal_server_error(e):
    """Internal server error."""
    response = {
        'title': '500 Internal Server Error',
        'message': '500 Internal Server Error',
    }

    return render_template('sys.jinja', **response), 500
