# DATABASE
# https://pythonhosted.org/Flask-SQLAlchemy/config.html
SQLALCHEMY_DATABASE_URI = ('postgresql://{username}:{password}@{hostandport}/'
    '{database}'.format(
        username='<USER>',
        password='<PASSWORD>',
        hostandport='localhost',
        database='<DATABASE>'
    ))


