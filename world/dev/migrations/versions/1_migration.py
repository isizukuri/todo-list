from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
jobs = Table('jobs', pre_meta,
    Column('description', VARCHAR(length=128)),
    Column('created_at', TIMESTAMP, primary_key=True, nullable=False),
    Column('finished_at', TIMESTAMP),
    Column('is_finished', BOOLEAN),
    Column('creator', INET, primary_key=True, nullable=False),
)

jobs = Table('jobs', post_meta,
    Column('description', String(length=128)),
    Column('created_at', TIMESTAMP, primary_key=True, nullable=False),
    Column('finished_at', TIMESTAMP),
    Column('is_finished', Boolean, default=ColumnDefault(False)),
    Column('user_ip', INET, primary_key=True, nullable=False),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['jobs'].columns['creator'].drop()
    post_meta.tables['jobs'].columns['user_ip'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['jobs'].columns['creator'].create()
    post_meta.tables['jobs'].columns['user_ip'].drop()
