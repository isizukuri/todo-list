TODO List
======

This site is designed as a test task for SoftServe.

**Author:** ragnarok49@gmail.com

**Source:** https://bitbucket.org/isizukuri/todo-list 

**Result:** http://ragnarok49.ddns.net


==========
Deployment
==========

*It is assumed that you have already installed and configured: Server OS (Debian Linux 8), Nginx, Supervisor, Git, specific tools (i.e tmux, mc, vi/vim, etc) - all that is necessary for the server work.*

***************
Server settings
***************

*P.s. OS Linux Debian 8.*

++++++++++++++++
Python and tools
++++++++++++++++

``$ sudo apt-get install python3``

``$ sudo apt-get install python3-venv``

``$ sudo apt-get install build-essential python3-dev libpq-dev``


++++++++++
PostgreSQL
++++++++++
``sudo apt-get install postgresql``

    Create database

    *For example: user - ``todolist``, password - ``todolist``, database - ``todolist``:*

    ``$ sudo -u postgres psql``

    .. code::

        # CREATE USER todolist WITH password 'todolist';
        # drop database if exists todolist;
        # CREATE DATABASE todolist;
        # GRANT ALL privileges ON DATABASE todolist TO todolist;
        # \q

****************
Project settings
****************

+++++++++++++++++++
Virtual environment
+++++++++++++++++++

*Go to the root directory of the project.*

``$ pyvenv venv``

``(venv): $ source venv/bin/activate``

``(venv): $ pip3 install -r requirements.txt``

++++++++++++++++++++
Local Flask Settings
++++++++++++++++++++

Copy the ``world/usr/options/local_settings.py.ex`` to the ``world/etc/local_settings.py`` and configure it:

.. code::

    # DATABASE
    # https://pythonhosted.org/Flask-SQLAlchemy/config.html
    SQLALCHEMY_DATABASE_URI = ('postgresql://{username}:{password}@{hostandport}/'
        '{database}'.format(
            username='todolist',
            password='todolist',
            hostandport='localhost',
            database='todolist'
        ))

+++++++++++++++++++
Deployment Settings
+++++++++++++++++++

Copy the ``world/usr/options/local_settings.sh.ex`` to the ``world/etc/local_settings.sh`` and configure it:

 - Project name
 - Host
 - Port

*P.s. Follow the instructions in the file.*


******
Deploy
******
The first time you need to run the project as:

*Go to the ./src/ directory.*
``(venv): $ ./manage.py createdb``

*Go back to the root directory.*

``cd ..``

``(venv): make deployserver``

*P.s.After the ``make deployserver`` you will see the instructions for setting up the server and supervisor.*

Use:
 - ``world/etc/nginx.conf`` for setting Nginx
 - ``world/etc/supervisor.conf`` for Supervisor settings.

After you configure ``Nginx`` and ``Supervisor``, you can use:
 - ``make start`` - start project
 - ``make stop`` - stop project
 - ``make restart`` - restart project

*******************
Test Run of Project
*******************

The first time you need to run the project as:

*Go to the ./src/ directory.*
 - Create database: ``(venv): $ ./manage.py createdb``
 - Migrate structure: ``(venv): $ ./manage.py migrate``
 - Run test server: ``(venv): $ ./manage.py runserver``

----
ToDo
----

Need finish ``collectstatic`` function for ``manage.py`` tools (i.e. for example use: ``Flask-Collect``). For this reason, as a temporary solution, make a hard link from the ``/path/to/todo-list/src/website/static`` to ``/path/to/todo-list/world/var/www`` - for correct work of Nginx webserver.

